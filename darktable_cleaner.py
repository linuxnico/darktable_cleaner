#!/usr/bin/env python3
# -*- coding:utf-8 -*-
# version 1
__version__ = "1.0"


import os
import sqlite3
import shutil


def verify(file):
    """verify

    Arguments:
        file : file to test

    Returns:
        tes if file exist
    """
    if os.path.exists(file):
        return True
    return False


def traitement(db, delete=False, sg=''):
    """traitement

    check file in db for exist on disc

    Arguments:
        db -- db file name with path
    """
    connection = sqlite3.connect(db)
    cur = connection.cursor()

    # retrieve films rolls number and path
    res = cur.execute("SELECT id,folder FROM film_rolls")
    film_rolls_temp = res.fetchall()

    # test existance of error_rolls
    error_roll = [fr for fr in film_rolls_temp if 'error_roll' in fr[1]]
    if error_roll == []:  # if error_rolls not exist, create
        error_rolls = int(film_rolls_temp[-1][0])+1
        requete = f"INSERT INTO film_rolls (id, access_timestamp, folder) VALUES({error_rolls}, 0, 'error_rolls/')"
        res = cur.execute(requete)
        connection.commit()

    else:  # else retrieve id of error_rolls
        error_rolls = int(error_roll[0][0])

    # list to dict for simplify usage
    film_rolls = {frt[0]: frt[1] for frt in film_rolls_temp}

    # retrieve image film rolls and name
    res = cur.execute("SELECT id,film_id,filename FROM images")
    taberror = {}
    ind = 0
    while True:
        idfilm_filename = res.fetchone()
        if not idfilm_filename:  # the end of data, nothing to do, break
            break
        id, idfilm, filename = idfilm_filename  # get infos
        if idfilm == error_rolls:  # already in roll error_rolls, continue
            continue
        # film do no exists so put on error tab
        if not verify(film_rolls[idfilm] + '/'+filename):
            taberror[id] = [idfilm, filename]
        ind += 1
    if len(taberror) == 0:  # nothing to do
        connection.commit()
        connection.close()
        return 0

    if delete:  # are you sure?
        ret = sg.popup_yes_no("Are you sure to delete:\n" +
                              '\n'.join([taberror[info][1] for info in taberror]))
        if not ret:
            delete = False
    for image in taberror:  # put image in error tab, in error roll
        if delete:  # delete
            cur.execute('DELETE FROM images WHERE id=?', [image])
        else:  # move
            cur.execute('UPDATE images SET film_id=?, filename=? WHERE id=?', [
                error_rolls, taberror[image][1], image])
    connection.commit()
    connection.close()
    return 1


def mainGui():
    """main function
    """
    import PySimpleGUI as sg
    sg.theme("DarkGrey13")
    layout = [[sg.FileBrowse(key='fichier', button_text="Open Library Db file",
                             enable_events=True, file_types=(("DB Files", "*.db"), ('All files', '*.*')))],
              [sg.Text("", key='db')],
              [sg.Checkbox(
                  key='del', text="Delete picture directly\n(No move to error roll)")],
              [sg.Button("Cleaning"), sg.Button(button_text="Quit")]
              ]
    window = sg.Window("Delete dead picture", layout, size=(250, 150),
                       resizable=True, element_justification="c")
    # Create an event loop
    while True:
        event, values = window.read()
        if event == 'fichier':
            window['db'].update(os.path.basename(values['fichier']))
            print(values['fichier'])
            window.refresh()
        if event == "Cleaning":
            fichier = values['fichier']
            shutil.copy2(fichier, fichier+'_old')
            ret = traitement(fichier, values['del'], sg)
            if ret:
                sg.popup("it's ok, file doesn't exist was modified/deleted")
            else:
                sg.popup("it's ok, nothing to do :-)")
        if event == sg.WIN_CLOSED or event == "Quit":
            break


if __name__ == "__main__":
    from sys import argv
    if len(argv) > 1:  # use argument cli
        traitement(argv[1])
    else:
        try:  # try gui
            mainGui()
        except Exception:
            print('Error, no gui, no cli...')
